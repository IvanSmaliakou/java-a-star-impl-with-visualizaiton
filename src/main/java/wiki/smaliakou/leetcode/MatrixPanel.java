package wiki.smaliakou.leetcode;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import com.sun.glass.events.KeyEvent;

import java.util.ArrayList;

class MatrixPanel extends JPanel implements KeyListener {
    private int[][] matrix;
    private ArrayList<Pair<Integer, Integer>> path;
    private Pair<Integer, Integer> start;
    private Pair<Integer, Integer> finish;
    private ArrayList<AStarNode> closedList;
    private Graphics g;
    private int i = 0;

    public MatrixPanel(int[][] matrix, ArrayList<Pair<Integer, Integer>> path, Pair<Integer, Integer> start,
            Pair<Integer, Integer> finish, ArrayList<AStarNode> closedList) {
        this.matrix = matrix;
        this.path = path;
        this.start = start;
        this.finish = finish;
        this.closedList = closedList;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        this.g = g;

        int rows = matrix.length;
        int cols = matrix[0].length;

        // calculate the size of each cell
        int cellWidth = getWidth() / cols;
        int cellHeight = getHeight() / rows;

        // draw the cells
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                int value = matrix[row][col];
                g.setColor(value == 0 ? Color.WHITE : Color.BLACK);
                if (row == this.start.getSecond() && col == this.start.getFirst()) {
                    g.setColor(Color.RED);
                } else if (row == this.finish.getSecond() && col == this.finish.getFirst()) {
                    g.setColor(Color.GREEN);
                }
                g.fillRect(col * cellWidth, row * cellHeight, cellWidth, cellHeight);
                g.setColor(Color.GREEN);
                g.drawLine(col * cellWidth, row * cellHeight, col * cellWidth + cellWidth, row * cellHeight);
                g.drawLine(col * cellWidth, row * cellHeight, col * cellWidth, row * cellHeight + cellHeight);
                g.drawLine(col * cellWidth, row * cellHeight + cellHeight, col * cellWidth + cellWidth,
                        row * cellHeight + cellHeight);
                g.drawLine(col * cellWidth + cellWidth, row * cellHeight, col * cellWidth + cellWidth,
                        row * cellHeight + cellHeight);
            }
        }
        if (this.i == this.closedList.size()) {
            g.setColor(Color.BLUE);
            for (int j = 1; j < path.size() - 1; j++) {
                g.fillRect((path.get(j).getFirst() * cellWidth) + 1, (path.get(j).getSecond() * cellHeight) + 1,
                        cellWidth - 1, cellHeight - 1);
            }
        }
        if (this.i > 0) {
            for (int j = i-1; j >= 0; j--) {
                g.setColor(Color.BLACK);
                g.setFont(new Font("TimesRoman", Font.PLAIN, 15));
                AStarNode node = this.closedList.get(j);
                char[] letterCharArr = Integer.toString(node.F).toCharArray();
                g.drawChars(letterCharArr, 0, letterCharArr.length,
                        (int) (cellWidth * node.cordinates.getFirst() + cellWidth * 0.3),
                        cellHeight * (node.cordinates.getSecond() + 1) - 3);

                letterCharArr = Integer.toString(node.G).toCharArray();
                g.drawChars(letterCharArr, 0, letterCharArr.length,
                        cellWidth * node.cordinates.getFirst() + 3,
                        cellHeight * node.cordinates.getSecond() + cellHeight / 3);

                letterCharArr = Integer.toString(node.H).toCharArray();
                g.drawChars(letterCharArr, 0, letterCharArr.length,
                        cellWidth * node.cordinates.getFirst() + cellWidth / 3 * 2,
                        cellHeight * node.cordinates.getSecond() + cellHeight / 3);
            }
        }
    }

    @Override
    public void keyTyped(java.awt.event.KeyEvent e) {}

    @Override
    public void keyPressed(java.awt.event.KeyEvent e) {
        this.i++;
        this.paintComponent(this.g);
        this.setVisible(false);
        this.setVisible(true);
    }

    @Override
    public void keyReleased(java.awt.event.KeyEvent e) {}
}
