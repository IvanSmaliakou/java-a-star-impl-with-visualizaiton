package wiki.smaliakou.leetcode;

import java.util.ArrayList;

public class App {

    public static void main(String[] args) {
        CustomGridLayout cgl = new CustomGridLayout();
        int[][] matrix = {
                { 0, 0, 0, 0, 0, 1, 0, 0 },
                { 0, 0, 0, 0, 0, 1, 0, 0 },
                { 0, 1, 0, 0, 0, 1, 0, 0 },
                { 0, 1, 0, 0, 0, 1, 0, 0 },
                { 0, 0, 0, 0, 0, 1, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 1, 0, 0, 0, 0, 0, 0 },
                { 0, 1, 0, 0, 0, 0, 0, 0 }
        };
        AStar aStarSvc = new AStar();
        Pair<Integer, Integer> start = new Pair<Integer, Integer>(0, 0);
        Pair<Integer, Integer> finish = new Pair<Integer, Integer>(7, 0);
        Pair<ArrayList<Pair<Integer, Integer>>, ArrayList<AStarNode>> res = aStarSvc.FindPath(matrix, start.getFirst(),
                start.getSecond(), finish.getFirst(), finish.getSecond());
        cgl.Render(matrix, res.getFirst(), start, finish, res.getSecond());
    }
}
