package wiki.smaliakou.leetcode;

class Pair<T1, T2> {
    private T1 first;
    private T2 second;

    Pair(T1 v1, T2 v2) {
        this.first = v1;
        this.second = v2;
    }

    public T1 getFirst() {
        return this.first;
    }

    public T2 getSecond() {
        return this.second;
    }

    @Override
    public boolean equals(Object obj2) {
        Pair<T1, T2> obj2Casted = (Pair<T1, T2>) obj2;
        return obj2Casted.getFirst() == this.first && obj2Casted.getSecond() == this.second;
    }

    @Override
    public int hashCode() {
        return this.first.hashCode() + (this.second.hashCode() * 2);
    }
}