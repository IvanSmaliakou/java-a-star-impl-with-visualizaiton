package wiki.smaliakou.leetcode;

public class AStarNode {
    public Pair<Integer, Integer> cordinates;
    public int G;
    public int H;
    public int F;

    public AStarNode(int G, Pair<Integer, Integer> cordinates, Pair<Integer, Integer> finishCordinates) {
        this.G = G;
        this.cordinates = cordinates;
        this.H = this.calculateH(finishCordinates);
        this.F = this.H + this.G;
    }

    private int calculateH(Pair<Integer, Integer> finishCordinates) {
        int absX = finishCordinates.getFirst() - this.cordinates.getFirst();
        if (absX < 0) {
            absX = -absX;
        }
        int absY = finishCordinates.getSecond() - this.cordinates.getSecond();
        if (absY < 0) {
            absY = -absY;
        }
        return absX + absY;
    }

    // Compares only cordinates.
    @Override
    public boolean equals(Object o2){
        AStarNode second = (AStarNode)o2;
        return second.cordinates.equals(this.cordinates);
    }
}