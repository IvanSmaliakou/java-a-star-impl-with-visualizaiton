package wiki.smaliakou.leetcode;

import javax.swing.*;
import java.util.ArrayList;

public class CustomGridLayout {
  public void Render(int[][] matrix, ArrayList<Pair<Integer, Integer>> path, Pair<Integer, Integer> start,
      Pair<Integer, Integer> finish, ArrayList<AStarNode> closedList) {
    MatrixPanel panel = new MatrixPanel(matrix, path, start, finish, closedList);
    JFrame frame = new JFrame("A* algorithm");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.add(panel);
    frame.addKeyListener(panel);
    frame.setSize(500, 500);
    frame.setVisible(true);
  }
}
