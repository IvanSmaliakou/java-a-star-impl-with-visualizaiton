package wiki.smaliakou.leetcode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class AStar {
    public Pair<ArrayList<Pair<Integer, Integer>>, ArrayList<AStarNode>> FindPath(int[][] matrix, int startX,
            int startY, int finishX,
            int finishY) {
        Pair<Integer, Integer> finish = new Pair<Integer, Integer>(finishX, finishY);
        HashMap<Pair<Integer, Integer>, Pair<Integer, Integer>> cameFrom = new HashMap<>();
        ArrayList<AStarNode> openList = new ArrayList<>();
        ArrayList<AStarNode> closedList = new ArrayList<>();
        HashMap<Pair<Integer, Integer>, Integer> cordinatesToF = new HashMap<>();
        openList.add(new AStarNode(0, new Pair<Integer, Integer>(startX, startY),
                new Pair<Integer, Integer>(finishX, finishY)));
        cameFrom.put(new Pair<Integer, Integer>(startX, startY), null);
        while (!openList.isEmpty()) {
            AStarNode current = openList.remove(0);
            closedList.add(current);
            cordinatesToF.put(current.cordinates, current.F);

            if (current.cordinates.equals(finish)) {
                break;
            }
            if (current.cordinates.getFirst() > 0
                    && matrix[current.cordinates.getSecond()][current.cordinates.getFirst() - 1] == 0) {
                AStarNode leftNeighbour = new AStarNode(current.G + 1,
                        new Pair<Integer, Integer>(current.cordinates.getFirst() - 1,
                                current.cordinates.getSecond()),
                        finish);
                int indexOpen = openList.indexOf(leftNeighbour);
                if (indexOpen > -1) {
                    AStarNode leftItemPrev = openList.get(indexOpen);
                    if (leftItemPrev.F > leftNeighbour.F) {
                        openList.set(indexOpen, leftNeighbour);
                        this.sortItemToLowerIndexByF(openList, indexOpen);
                        cameFrom.put(leftNeighbour.cordinates, current.cordinates);
                    }
                } else {
                    if (!cordinatesToF.containsKey(leftNeighbour.cordinates)
                            || cordinatesToF.get(leftNeighbour.cordinates) > leftNeighbour.F) {
                        openList.add(leftNeighbour);
                        this.sortItemToLowerIndexByF(openList, openList.size() - 1);
                        cameFrom.put(leftNeighbour.cordinates, current.cordinates);
                    }
                }
            }
            if (current.cordinates.getFirst() < matrix[0].length - 1
                    && matrix[current.cordinates.getSecond()][current.cordinates.getFirst() + 1] == 0) {
                AStarNode rightNeighbor = new AStarNode(current.G + 1,
                        new Pair<Integer, Integer>(current.cordinates.getFirst() + 1,
                                current.cordinates.getSecond()),
                        finish);
                int indexOpen = openList.indexOf(rightNeighbor);
                if (indexOpen > -1) {
                    AStarNode rightItemPrev = openList.get(indexOpen);
                    if (rightItemPrev.F > rightNeighbor.F) {
                        openList.set(indexOpen, rightNeighbor);
                        this.sortItemToLowerIndexByF(openList, indexOpen);
                        cameFrom.put(rightNeighbor.cordinates, current.cordinates);
                    }
                } else {
                    if (!cordinatesToF.containsKey(rightNeighbor.cordinates)
                            || cordinatesToF.get(rightNeighbor.cordinates) > rightNeighbor.F) {
                        openList.add(rightNeighbor);
                        this.sortItemToLowerIndexByF(openList, openList.size() - 1);
                        cameFrom.put(rightNeighbor.cordinates, current.cordinates);
                    }
                }
            }
            if (current.cordinates.getSecond() > 0
                    && matrix[current.cordinates.getSecond() - 1][current.cordinates.getFirst()] == 0) {
                AStarNode upNeighbor = new AStarNode(current.G + 1,
                        new Pair<Integer, Integer>(current.cordinates.getFirst(),
                                current.cordinates.getSecond() - 1),
                        finish);
                int indexOpen = openList.indexOf(upNeighbor);
                if (indexOpen > -1) {
                    AStarNode upItemPrev = openList.get(indexOpen);
                    if (upItemPrev.F > upNeighbor.F) {
                        openList.set(indexOpen, upNeighbor);
                        this.sortItemToLowerIndexByF(openList, indexOpen);
                        cameFrom.put(upNeighbor.cordinates, current.cordinates);
                    }
                } else {
                    if (!cordinatesToF.containsKey(upNeighbor.cordinates)
                            || cordinatesToF.get(upNeighbor.cordinates) > upNeighbor.F) {
                        openList.add(upNeighbor);
                        this.sortItemToLowerIndexByF(openList, openList.size() - 1);
                        cameFrom.put(upNeighbor.cordinates, current.cordinates);
                    }
                }
            }
            if (current.cordinates.getSecond() < matrix.length - 1
                    && matrix[current.cordinates.getSecond() + 1][current.cordinates.getFirst()] == 0) {
                AStarNode downNeighbor = new AStarNode(current.G + 1,
                        new Pair<Integer, Integer>(current.cordinates.getFirst(),
                                current.cordinates.getSecond() + 1),
                        finish);
                int indexOpen = openList.indexOf(downNeighbor);
                if (indexOpen > -1) {
                    AStarNode downItemPrev = openList.get(indexOpen);
                    if (downItemPrev.F > downNeighbor.F) {
                        openList.set(indexOpen, downNeighbor);
                        this.sortItemToLowerIndexByF(openList, indexOpen);
                        cameFrom.put(downNeighbor.cordinates, current.cordinates);
                    }
                } else {
                    if (!cordinatesToF.containsKey(downNeighbor.cordinates)
                            || cordinatesToF.get(downNeighbor.cordinates) > downNeighbor.F) {
                        openList.add(downNeighbor);
                        this.sortItemToLowerIndexByF(openList, openList.size() - 1);
                        cameFrom.put(downNeighbor.cordinates, current.cordinates);
                    }
                }
            }
        }

        ArrayList<Pair<Integer, Integer>> reversedPath = new ArrayList<>();
        Pair<Integer, Integer> current = finish;
        while (current != null) {
            reversedPath.add(current);
            current = cameFrom.get(current);
        }
        Collections.reverse(reversedPath);
        return new Pair<ArrayList<Pair<Integer, Integer>>, ArrayList<AStarNode>>(reversedPath, closedList);
    }

    private void sortItemToLowerIndexByF(ArrayList<AStarNode> arr, int itemIndex) {
        while (itemIndex > 0 && arr.get(itemIndex).F < arr.get(itemIndex - 1).F) {
            AStarNode tmp = arr.get(itemIndex);
            arr.set(itemIndex, arr.get(itemIndex - 1));
            arr.set(itemIndex - 1, tmp);
            itemIndex--;
        }
    }
}
