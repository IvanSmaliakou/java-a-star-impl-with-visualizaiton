package wiki.smaliakou.leetcode;

import java.awt.*;
import java.awt.event.ActionEvent;

import javax.swing.*;

import java.util.ArrayList;

class ResultPanel extends JPanel {
    private ArrayList<AStarNode> closedList;
    private ArrayList<Pair<Integer, Integer>> resultPath;
    private int[][] matrix;
    public ResultPanel(int[][] matrix, Pair<ArrayList<Pair<Integer, Integer>>, ArrayList<AStarNode>> res){
        this.matrix = matrix;
        this.resultPath = res.getFirst();
        this.closedList = res.getSecond();
    }
}